# Copy of the sources of Umbra

This product is based on Umbra, Copyright (c) 2001 by Mark Hughes, which may be found at <http://kuoi.asui.uidaho.edu/~kamikaze/Umbra/>.

This source is currently just a copy of the sources of Umbra at http://markdamonhughes.com/Umbra/ by Mark Damon Hughes.
However, it can become altered at which point it will also be marked clearly to be different.

It is published under a [custom license](license.txt).




